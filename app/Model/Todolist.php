<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Todolist extends Model
{
    protected $table = 'todolist';
    public $timestamps = false;

    protected $fillable =[
        'id' ,
        'title',
        'is_completed',
    ];

    public static function creatAddTodo($data)
    {
        try{
            $ret = DB::table('todolist')->insert($data);
        }catch (\Exception $e){

            return false;
        }

        return $ret;
    }


    public static function deleteAddTodo($data)
    {
        $ret = Todolist::whereIn('id', $data) ->delete();
        if (empty($ret)) {

            return false;
        }

       return true;
    }

    public static function getSearchTodoALL($offset, $limit)
    {
        $count = Todolist::count();
        $ret = Todolist::offset($offset)->limit($limit)->get()->toArray();

        return [
            "count" => $count,
            "data" => $ret
        ];
    }

    public static function getSearchTodoActive($offset, $limit)
    {
        $count = Todolist::where('is_completed', '=', 0)->count();
        $ret = Todolist::where('is_completed', '=', 0)
            ->offset($offset)->limit($limit)
            ->get()
            ->toArray();

        return [
            "count" => $count,
            "data" => $ret
        ];
    }

    public static function getSearchTodoCompleted($offset, $limit)
    {
        $count = Todolist::where('is_completed', '=', 1)->count();
        $ret = Todolist::where('is_completed', '=', 1 )
            ->offset($offset)
            ->limit($limit)
            ->get()
            ->toArray();

        return [
            "count" => $count,
            "data" => $ret
        ];
    }

    public static function putEditTodo($data)
    {
        try{
            Todolist::where('id', '=', $data['id'])->update($data);
        } catch (\Exception $e) {

            return false;
        }

        return true;
    }
}