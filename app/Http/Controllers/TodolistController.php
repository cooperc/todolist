<?php

namespace App\Http\Controllers;

use App\Model\Todolist;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class TodolistController extends BaseController
{
    use  ValidatesRequests;

    // 新增Todo
    public function addTodo(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {

            return [
                'error' => "EMPTY_INPUT"
            ];
        }

        $insert_db_data = [];
        foreach ($data as $index => $value) {
            $insert_db_data[$index]['title'] = $value;
            $insert_db_data[$index]['is_completed'] = 0;
        }

        $is_success = Todolist::creatAddTodo($insert_db_data);
        if (!$is_success){
           return [
               'msg' => "failure"

           ];
        }

        return[
            'msg' => 'success'
        ];
    }

    // 刪除Todo
    public function deleteTodo(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {

            return [
                'error' => "EMPTY_INPUT"
            ];
        }

        $is_success = Todolist::deleteAddTodo($data);
        if (!$is_success){
            return [
                'msg' => "nothing delete"

            ];
        }

        return[
            'msg' => 'success'
        ];
    }

    // 搜尋Todo
    public function searchTodo($type, Request $request)
    {
        $define_search_type = ['ALL', 'ACTIVE', 'COMPLETED'];
        $upper_type = strtoupper($type);
        if (!in_array($upper_type, $define_search_type)){

            return [
                'error' => "ERROR_SEARCH_TYPE"
            ];
        }

        // 預設一次顯示10筆資料
        $current_page = (int)$request->input('current_page', 1);
        $num_per_page = (int)$request->input('num_per_page', 10);

        // 分頁
        $limit = $num_per_page; //一頁幾筆資料
        $offset = ($current_page - 1)* $limit; // DB從哪個位置開始找


        if ($upper_type == "ALL") {
            $db_data = Todolist::getSearchTodoALL($offset, $limit);
        }

        if ($upper_type == "ACTIVE"){
            $db_data = Todolist::getSearchTodoActive($offset, $limit);
        }

        if($upper_type == "COMPLETED"){
            $db_data = Todolist::getSearchTodoCompleted($offset, $limit);
        }

        // 無條件進位
        $total_page = ceil($db_data['count']  / $num_per_page );

        return [
            'total_page' => $total_page,
            'data' => $db_data['data']
        ];
    }

    // 編輯
    public static function editTodo(Request $request)
    {

        if (!$request->has('id')) {

            return [
                'error' => "MISSING_ID"
            ];
        }
        
        if (!$request->has('title')) {

            return [
                'error' => "MISSING_TITLE"
            ];
        } else{
            if (empty($request->input('title'))) {

                return [
                    'error' => "EMPTY_TITLE"
                ];
            }
        }

        if (!$request->has('is_completed')) {

            return [
                'error' => "MISSING_COMPLETED"
            ];
        }

        $data = $request->all();
        $is_success = Todolist::putEditTodo($data);

        if (!$is_success){
            return [
                'msg' => "failure"

            ];
        }

        return[
            'msg' => 'success'
        ];

    }
}