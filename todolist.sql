-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- 主機： db
-- 產生時間： 2022 年 03 月 12 日 08:44
-- 伺服器版本： 5.6.51
-- PHP 版本： 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫: `todolist`
--

-- --------------------------------------------------------

--
-- 資料表結構 `todolist`
--

CREATE TABLE `todolist` (
  `id` int(11) NOT NULL COMMENT 'id',
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '標題',
  `is_completed` int(1) NOT NULL COMMENT '是否完成'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 傾印資料表的資料 `todolist`
--

INSERT INTO `todolist` (`id`, `title`, `is_completed`) VALUES
(6, 'AAA', 0),
(7, 'BBB', 0),
(8, 'CCC', 0),
(9, 'DDD', 0),
(10, 'EEE', 0),
(11, 'AAA', 1),
(12, 'BBB', 1),
(13, 'CCC', 1),
(14, 'DDD', 1),
(15, 'EEE', 1),
(16, 'FFF', 1),
(17, '買便當', 1),
(18, '買飲料', 0),
(19, '洗衣服', 0),
(20, '逛網拍', 0),
(21, '買衣服', 0);

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `todolist`
--
ALTER TABLE `todolist`
  ADD PRIMARY KEY (`id`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `todolist`
--
ALTER TABLE `todolist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
