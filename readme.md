## Todolist 
----

### DB Tables
| 欄位名稱          |       格式      |    說明       | 備註 |
| ---------------- | -------------- | ------------- | ---- |
|  id                 | int         |     ID        |        |
| title               | varchar     |    標題        |         |
| is_completed        |  int        |   是否完成      | 0 代表未完成，1 代表完成|

----
### API格式
| Method       |       URL                              |    說明             | 備註    |
| -------------- | --------------------------------- | ------------------ | -------- |
|  POST          | /api/todo/add                   |     新增 todo     |         |
|  POST          |  /api/todo/delete               |    刪除 todo   |         |
| PUT            |   /api/todo/edit                  |    編輯 todo    |        |
| GET            |   /api/todo/search/{type}    |   搜尋 todo     |  type：all (全部) / active (進行中) / completed (已完成) |

----
#### 新增 todo 範例

URL：
```
[POST] http://localhost:9090/api/todo/add
```
參數：

| 名稱  | 型態 |
| ------ | ------------- |
| title     | array  |


```
body：
[
	"買便當",
	"洗衣服",
	"買衛生紙",
	"叫外賣",
]
```
回傳：

```
成功：
{
	"msg": "success"
}
	
失敗：
{
	"msg": "failure"
}
```

----
#### 刪除 todo 範例

URL：
```
[POST] http://localhost:9090/api/todo/delete
```
ex：參數：

| 名稱  | 型態 |
| ------ | ------------- |
| id     | array  |

```
ex：
[
	"1",  // id=1
	"2",
	"3",
	"4",
	"5"
]
```
回傳：

```
成功：
{
	"msg": "success"
}
	
失敗：
{
	"error": "EMPTY_INPUT" // 陣列攜帶空值
}
```

----
#### 編輯 todo 範例

URL：
```
[PUT] http://localhost:9090/api/todo/edit
```
body參數：

| 名稱  | 型態 |
| ------ | ------------- |
| id                  | int  |
| title              | string  |
| is_completed | boolean  |


```
ex：：
{
    "id":2,
    "title": "超商買飲料",
    "is_completed": false
}
```
回傳：

```
成功：
{
	"msg": "success"
}
	
失敗：
{
	"msg": "failure"
}
```

----
#### 搜尋  todo 範例

URL參數：

| 名稱  |  搜尋種類 |
| ------ | ------------ |
| type   | all (全部),  active (進行中), completed (已完成) |

URL：
```
[GET] http://localhost:9090/api/todo/search/{type}

ex:
http://localhost:9090/api/todo/search/active
```

body參數：

| 名稱  | 型態    |  搜尋種類 |
| ------ | -------- | ------------ |
| current_page   | int    | 當前頁數|
| num_per_page   | int  | 每頁幾筆資料 |

```
ex：
{
	"current_page":1,
	"num_per_page":5
}
```
回傳：

```
{
    "total_page": 2, // 總頁數
    "data": [
        {
            "id": 11,
            "title": "買便當",
            "is_completed": 0
        },
        {
            "id": 12,
            "title": "洗衣服",
            "is_completed": 1
        },
        {
            "id": 13,
            "title": "買衛生紙",
            "is_completed": 1
        },
        {
            "id": 14,
            "title": "叫外賣",
            "is_completed": 1
        },
        {
            "id": 15,
            "title": "EEE",
            "is_completed": 1
        }
    ]
}
```